use std::ops::RangeInclusive;

use cgmath::Point2;

use crate::ball;
use crate::ball::Ball;
use crate::gameinfo::GameInfo;
use crate::gameinfo::GameState;
use crate::gameinfo::HEIGHT;
use crate::gameinfo::WIDTH;

pub const RADIUS: i32 = 40;
pub const END_LIMIT: f64 = (WIDTH / 2 - RADIUS) as f64;
pub const CENTER_LIMIT: f64 = RADIUS as f64;

#[derive(Copy, Clone, Debug)]
pub enum TeamColor {
    Blue,
    Red,
}

impl TeamColor {
    pub fn slime(&self) -> Slime {
        match self {
            TeamColor::Blue => Slime {
                color: TeamColor::Blue,
                vx: 0.0,
                position: -END_LIMIT,
            },
            TeamColor::Red => Slime {
                color: TeamColor::Red,
                vx: 0.0,
                position: END_LIMIT,
            },
        }
    }

    pub fn range(&self) -> RangeInclusive<f64> {
        match self {
            TeamColor::Blue => -END_LIMIT..=-CENTER_LIMIT,
            TeamColor::Red => CENTER_LIMIT..=END_LIMIT,
        }
    }

    pub fn service(&self) -> Ball {
        let x = (WIDTH / 2) as f64 - ball::SERVE_OFFSET;
        Ball::new_stop(
            match self {
                TeamColor::Blue => -x,
                TeamColor::Red => x,
            },
            (HEIGHT - 20) as f64,
        )
    }

    pub fn move_score(&self, game_info: &mut GameInfo) {
        match self {
            TeamColor::Blue => {
                game_info.score_blue += 1;
                game_info.score_red -= 1;
                if game_info.score_red == 0 {
                    game_info.state = GameState::WonByBlue;
                }
            }
            TeamColor::Red => {
                game_info.score_red += 1;
                game_info.score_blue -= 1;
                if game_info.score_blue == 0 {
                    game_info.state = GameState::WonByRed;
                }
            }
        }
    }
    pub fn set_service(&self, game_info: &mut GameInfo) {
        match self {
            TeamColor::Blue => {
                game_info.state = GameState::ServiceByBlue;
            }
            TeamColor::Red => {
                game_info.state = GameState::ServiceByRed;
            }
        }
    }
}

#[derive(Debug)]
pub struct Slime {
    pub color: TeamColor,
    vx: f64,
    position: f64,
}

impl Slime {
    pub fn point(&self) -> Point2<f64> {
        Point2::new(self.position as f64, 0.0)
    }
    pub fn go_right(&self) -> Slime {
        Slime { vx: 1.0, ..*self }
    }
    pub fn go_left(&self) -> Slime {
        Slime { vx: -1.0, ..*self }
    }

    pub fn next(&self) -> Slime {
        let new_position = self.position + self.vx;
        Slime {
            position: if new_position > *self.color.range().end() {
                *self.color.range().end()
            } else if new_position < *self.color.range().start() {
                *self.color.range().start()
            } else {
                new_position
            },
            ..*self
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::slime::TeamColor;
    use crate::slime::CENTER_LIMIT;
    use crate::slime::END_LIMIT;

    #[test]
    fn test_go_end() {
        let slime = TeamColor::Blue.slime();
        assert_eq!(slime.go_right().vx, 1.0);
    }

    #[test]
    fn test_go_center() {
        let slime = TeamColor::Blue.slime();
        assert_eq!(slime.go_left().vx, -1.0);
    }

    #[test]
    fn test_check_left_limit() {
        let slime = TeamColor::Blue.slime();
        assert_eq!(slime.go_left().next().position, -END_LIMIT);
    }

    #[test]
    fn test_check_right_limit() {
        let slime = TeamColor::Blue.slime();
        let mut slime2 = slime.go_right();
        slime2.position = -CENTER_LIMIT;
        assert_eq!(slime2.next().position, -CENTER_LIMIT);
    }
}
