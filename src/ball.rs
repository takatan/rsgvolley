use cgmath::prelude::*;
use cgmath::vec2;
use cgmath::Matrix2;
use cgmath::Point2;
use cgmath::Vector2;

use crate::gameinfo::WIDTH;
use crate::gamewin;
use crate::slime;
use crate::slime::Slime;

const GRAVITY: f64 = 0.03;
pub const RADIUS: i32 = 10;
pub const SERVE_OFFSET: f64 = 60.0;

#[derive(Debug)]
pub struct Ball {
    pub p: Point2<f64>,
    pub v: Vector2<f64>,
}

impl Ball {
    const REFLECT: Matrix2<f64> = Matrix2::new(-1.0, 0.0, 0.0, 1.0);

    pub fn new(x: f64, y: f64, vx: f64, vy: f64) -> Ball {
        Ball {
            p: Point2::new(x, y),
            v: vec2(vx, vy),
        }
    }
    pub fn new_stop(x: f64, y: f64) -> Ball {
        Ball::new(x, y, 0.0, 0.0)
    }

    pub fn next(&self) -> Ball {
        Ball {
            p: self.p + self.v,
            v: self.v + vec2(0.0, -GRAVITY),
        }
    }

    pub fn is_collision_wall(&self) -> bool {
        self.next().p.x.abs() > self.p.x.abs()
            && self.next().p.x.abs() > (WIDTH / 2 - RADIUS) as f64
    }

    pub fn reflect_wall(&self) -> Ball {
        Ball {
            p: self.p,
            v: Ball::REFLECT * self.v,
        }
    }

    pub fn is_collision_slime(&self, slime: &Slime) -> bool {
        let l = self.p.distance(slime.point());
        let l2 = self.next().p.distance(slime.point());
        l2 < l && l < (slime::RADIUS + RADIUS) as f64
    }

    pub fn reflect_slime(&self, slime: &Slime) -> Ball {
        Ball {
            v: Matrix2::from_angle(Vector2::unit_x().angle(self.p - slime.point()) * 2.0)
                * Ball::REFLECT
                * self.v
                + vec2(0.0, -GRAVITY),
            //            vx: -angle.cos() * self.vx - angle.sin() * self.vy,
            //            vy: -angle.sin() * self.vx + angle.cos() * self.vy,
            ..*self
        }
    }

    pub fn is_collision_penalty(&self, p: Point2<f64>) -> bool {
        p.distance(self.p) < gamewin::PENALTY_RADIUS + RADIUS as f64
    }
}

#[cfg(test)]
mod tests {
    use nearly_eq::*;

    use crate::ball::Ball;
    use crate::ball::GRAVITY;
    use crate::slime::TeamColor;

    #[test]
    fn test_next() {
        let ball = Ball::new_stop(100_f64, 200.0);
        let ball2 = ball.next();
        assert_eq!(ball2.p.y, ball.p.y);
        assert_eq!(ball2.v.y, -1.0 * GRAVITY);
        let ball3 = ball2.next();
        assert_eq!(ball3.p.y, ball.p.y - 1.0 * GRAVITY);
        assert_eq!(ball3.v.y, -2.0 * GRAVITY);
    }

    #[test]
    fn test_reflect_wall() {
        let ball = Ball::new(100.0, 100.0, 10.0, -5.0);
        let reflect = ball.reflect_wall();
        assert_eq!(reflect.v.x, -10.0);
        assert_eq!(reflect.v.y, -5.0);
    }

    #[test]
    fn test_reflect_slime() {
        let slime = TeamColor::Red.slime();
        let ball = Ball::new(slime.point().x, 100.0, 0.0, -10.0);
        let reflect = ball.reflect_slime(&slime);
        assert_nearly_eq!(reflect.v.x, 0.0);
        assert_nearly_eq!(reflect.v.y, 10.0 - GRAVITY);

        let ball = Ball::new(slime.point().x, 100.0, 10.0, -10.0);
        let reflect = ball.reflect_slime(&slime);
        assert_nearly_eq!(reflect.v.x, 10.0);
        assert_nearly_eq!(reflect.v.y, 10.0 - GRAVITY);

        let ball = Ball::new(slime.point().x + 100.0, 100.0, 0.0, -10.0);
        let reflect = ball.reflect_slime(&slime);
        assert_nearly_eq!(reflect.v.x, 10.0);
        assert_nearly_eq!(reflect.v.y, 0.0 - GRAVITY);

        let ball = Ball::new(slime.point().x - 100.0, 100.0, 0.0, -10.0);
        let reflect = ball.reflect_slime(&slime);
        assert_nearly_eq!(reflect.v.x, -10.0);
        assert_nearly_eq!(reflect.v.y, 0.0 - GRAVITY);

        let ball = Ball::new(slime.point().x + 100.0, 100.0 * (3_f64).sqrt(), 0.0, -10.0);
        let reflect = ball.reflect_slime(&slime);
        assert_nearly_eq!(reflect.v.x, 5.0 * (3_f64.sqrt()));
        assert_nearly_eq!(reflect.v.y, 5.0 - GRAVITY);
    }
}
