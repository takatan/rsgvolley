use std::f64;

use cairo::Context;
use cairo::SolidPattern;
use cgmath::Point2;

use crate::ball;
use crate::ball::Ball;
use crate::gameinfo;
use crate::gameinfo::GameInfo;
use crate::gameinfo::GameState;
use crate::gameinfo::WIDTH;
use crate::slime;
use crate::slime::Slime;
use crate::slime::TeamColor;

pub const PENALTY_RADIUS: f64 = 30.0;

pub trait WinGeo {
    fn win_x(&self) -> f64;
    fn win_y(&self) -> f64;
}

impl WinGeo for Point2<f64> {
    fn win_x(&self) -> f64 {
        self.x + WIDTH as f64 / 2.0
    }

    fn win_y(&self) -> f64 {
        (gameinfo::HEIGHT as f64) - self.y
    }
}

impl WinGeo for Ball {
    fn win_x(&self) -> f64 {
        self.p.win_x()
    }

    fn win_y(&self) -> f64 {
        self.p.win_y()
    }
}

impl WinGeo for Slime {
    fn win_x(&self) -> f64 {
        self.point().win_x()
    }

    fn win_y(&self) -> f64 {
        self.point().win_y()
    }
}

pub struct GameWin<'a> {
    cr: &'a Context,
    pattern_blue: SolidPattern,
    pattern_red: SolidPattern,
    pattern_ball: SolidPattern,
    pattern_penalty: SolidPattern,
}

impl<'a> GameWin<'a> {
    pub fn new(cr: &'a Context) -> GameWin<'a> {
        cr.set_font_size(16.0);
        GameWin {
            cr,
            pattern_blue: SolidPattern::from_rgb(0.0, 0.0, 1.0),
            pattern_red: SolidPattern::from_rgb(1.0, 0.0, 0.0),
            pattern_ball: SolidPattern::from_rgb(1.0, 1.0, 1.0),
            pattern_penalty: SolidPattern::from_rgb(0.0, 1.0, 0.0),
        }
    }
    pub fn draw_game_win(&self, game_info: &GameInfo) {
        self.draw_back();
        self.draw_penalty(game_info.penalty);
        self.draw_slime(&game_info.blue);
        self.draw_slime(&game_info.red);
        self.draw_ball(&game_info.ball);
        self.draw_score(game_info);
        self.draw_winner(game_info);
    }
    fn draw_back(&self) {
        self.cr.set_source_rgb(0.1, 0.1, 0.1);
        self.cr
            .rectangle(0.0, 0.0, WIDTH as f64, gameinfo::HEIGHT as f64);
        self.cr.fill().unwrap();

        self.cr.set_source_rgb(1.0, 1.0, 1.0);
        self.cr.rectangle(
            (WIDTH as f64) / 2.0 - 1.0,
            120.0,
            2.0,
            gameinfo::HEIGHT as f64,
        );
        self.cr.fill().unwrap();
    }
    fn draw_slime(&self, slime: &Slime) {
        let pattern = match slime.color {
            TeamColor::Blue => &self.pattern_blue,
            TeamColor::Red => &self.pattern_red,
        };
        self.cr.set_source(pattern).unwrap();
        self.cr.move_to(slime.win_x(), slime.win_y());
        self.cr.arc(
            slime.win_x(),
            slime.win_y(),
            slime::RADIUS as f64,
            f64::consts::PI,
            0.0,
        );
        self.cr.fill().unwrap();
    }

    fn draw_ball(&self, ball: &Ball) {
        self.cr.set_source(&self.pattern_ball).unwrap();
        self.cr.move_to(ball.win_x(), ball.win_y());
        self.cr.arc(
            ball.win_x(),
            ball.win_y(),
            ball::RADIUS as f64,
            0.0,
            f64::consts::PI * 2.0,
        );
        self.cr.fill().unwrap();
    }
    fn draw_penalty(&self, p: Point2<f64>) {
        self.cr.set_line_width(5.0);
        self.cr.set_source(&self.pattern_penalty).unwrap();
        self.cr.arc(
            p.win_x(),
            p.win_y(),
            PENALTY_RADIUS,
            0.0,
            f64::consts::PI * 2.0,
        );
        self.cr.stroke_preserve().unwrap();

        self.cr.set_source_rgb(0.0, 0.0, 0.0);
        self.cr.fill().unwrap();

        self.cr.set_source(&self.pattern_penalty).unwrap();
        self.cr.arc(
            p.win_x(),
            p.win_y(),
            PENALTY_RADIUS - 15.0,
            0.0,
            f64::consts::PI * 2.0,
        );
        self.cr.set_line_width(5.0);
        self.cr.stroke_preserve().unwrap();

        self.cr.set_source_rgb(0.0, 0.0, 0.0);
        self.cr.fill().unwrap();
    }

    fn draw_score(&self, game_info: &GameInfo) {
        let buf = format!(
            "Blue: {} Ball: {} Red: {}",
            game_info.score_blue, game_info.ball_count, game_info.score_red
        );
        self.show_text_centering(buf.as_str(), 20)
    }

    fn draw_winner(&self, game_info: &GameInfo) {
        let winner = match game_info.state {
            GameState::WonByBlue => "BLUE",
            GameState::WonByRed => "RED",
            _ => return,
        };
        self.show_text_centering("PRESS R TO PLAY AGAIN", 80);
        self.show_text_centering(format!("WON BY {}", winner).as_str(), 60);
    }

    fn show_text_centering(&self, text: &str, y: i32) {
        let te = self.cr.text_extents(text).unwrap();
        self.cr.move_to((WIDTH as f64 - te.width()) / 2.0, y as f64);
        self.cr.show_text(text).unwrap();
    }
}
