use cgmath::Point2;
use rand::prelude::*;

use crate::ball::Ball;
use crate::slime::Slime;
use crate::slime::TeamColor;

pub const WIDTH: i32 = 640;
pub const HEIGHT: i32 = 400;
const SCORE_INIT: i32 = 50;

#[derive(PartialEq, Eq, Debug)]
pub enum GameState {
    Init,
    Play,
    GotByBlue,
    GotByRed,
    ServiceByBlue,
    ServiceByRed,
    WonByBlue,
    WonByRed,
}

#[derive(Debug)]
pub struct GameInfo {
    pub ball: Ball,
    pub blue: Slime,
    pub red: Slime,
    pub state: GameState,
    pub ball_owner: TeamColor,
    pub ball_count: i32,
    pub penalty: Point2<f64>,
    pub score_blue: i32,
    pub score_red: i32,
    pub wait_count: i32,
}

impl GameInfo {
    pub fn new() -> GameInfo {
        GameInfo {
            ball: TeamColor::Blue.service(),
            blue: TeamColor::Blue.slime(),
            red: TeamColor::Red.slime(),
            state: GameState::Init,
            ball_owner: TeamColor::Blue,
            ball_count: 0,
            penalty: Point2::new(0.0, 0.0),
            score_blue: SCORE_INIT,
            score_red: SCORE_INIT,
            wait_count: 0,
        }
    }

    pub fn init(&mut self) {
        self.state = GameState::ServiceByBlue;
    }

    pub fn serve_set(&mut self, server: TeamColor) {
        self.ball = server.service();
        self.blue = TeamColor::Blue.slime();
        self.red = TeamColor::Red.slime();
        self.penalty = Point2::new(0.0, thread_rng().gen_range(160.0..(HEIGHT - 40) as f64));
        self.ball_count = 1;
        self.wait_count = 0;
        self.state = GameState::Play;
    }

    pub fn score_move(&mut self, winner: TeamColor) {
        if self.wait_count > 0 {
            self.wait_count -= 1;
        } else if self.ball_count > 0 {
            self.ball_count -= 1;
            self.wait_count = if self.ball_count == 0 { 50 } else { 7 };
            winner.move_score(self);
        } else {
            winner.set_service(self);
        }
    }
    pub fn next(&mut self) {
        let ball_next = if self.ball.is_collision_wall() {
            self.ball.reflect_wall()
        } else if self.ball.is_collision_slime(&self.blue) {
            self.ball_count += 1;
            self.ball_owner = TeamColor::Blue;
            self.ball.reflect_slime(&self.blue)
        } else if self.ball.is_collision_slime(&self.red) {
            self.ball_count += 1;
            self.ball_owner = TeamColor::Red;
            self.ball.reflect_slime(&self.red)
        } else {
            self.ball.next()
        };
        if ball_next.is_collision_penalty(self.penalty) {
            if self.wait_count > 0 {
                self.wait_count -= 1;
            } else {
                match self.ball_owner {
                    TeamColor::Blue => {
                        self.score_blue -= 1;
                    }
                    TeamColor::Red => {
                        self.score_red -= 1;
                    }
                }
                self.wait_count = 2;
            }
        }
        self.ball = ball_next;
        self.blue = self.blue.next();
        self.red = self.red.next();
    }

    pub fn blue_left(&mut self) {
        self.blue = self.blue.go_left();
    }

    pub fn blue_right(&mut self) {
        self.blue = self.blue.go_right();
    }

    pub fn red_left(&mut self) {
        self.red = self.red.go_left();
    }

    pub fn red_right(&mut self) {
        self.red = self.red.go_right();
    }
}

#[cfg(test)]
mod tests {}
