use std::f64;
use std::sync::Arc;
use std::sync::Mutex;

use gtk::prelude::*;

use crate::gameinfo::{GameInfo, GameState};
use crate::slime::TeamColor;
use gtk::{Application, ApplicationWindow};
use std::time::Duration;

mod ball;
mod gameinfo;
mod gamewin;
mod slime;

fn main() {
    let app = Application::builder().build();
    app.connect_activate(|app| {
        let game_info = Arc::new(Mutex::new(GameInfo::new()));
        let window = ApplicationWindow::builder()
            .application(app)
            .title("First GTK+ Program")
            .resizable(false)
            .width_request(gameinfo::WIDTH)
            .height_request(gameinfo::HEIGHT)
            .app_paintable(true)
            .build();
        {
            let window = window.clone();
            let game_info = game_info.clone();
            let interval = Duration::from_millis(10);
            glib::timeout_add_local(interval, move || {
                let mut game_info = game_info.lock().unwrap();
                match game_info.state {
                    GameState::Init => {
                        game_info.init();
                    }
                    GameState::Play => {
                        game_info.next();
                        if game_info.ball.p.y < ball::RADIUS as f64 {
                            if game_info.ball.p.x < 0.0 {
                                game_info.state = GameState::GotByRed;
                            } else {
                                game_info.state = GameState::GotByBlue;
                            }
                        }
                    }
                    GameState::GotByBlue => {
                        game_info.score_move(TeamColor::Blue);
                    }
                    GameState::GotByRed => {
                        game_info.score_move(TeamColor::Red);
                    }
                    GameState::ServiceByBlue => {
                        game_info.serve_set(TeamColor::Blue);
                    }
                    GameState::ServiceByRed => {
                        game_info.serve_set(TeamColor::Red);
                    }
                    GameState::WonByBlue => {}
                    GameState::WonByRed => {}
                }
                window.queue_draw();
                Continue(true)
            });
        }
        {
            let field = game_info.clone();
            window.connect_draw(move |_, cr| {
                let field = field.lock().unwrap();
                let game_win = gamewin::GameWin::new(cr);
                game_win.draw_game_win(&field);
                Inhibit(false)
            });
        }

        {
            window.connect_key_press_event(move |_, ev| {
                let mut field = game_info.lock().unwrap();
                match ev.keyval() {
                    gdk::keys::constants::a => {
                        field.blue_left();
                    }
                    gdk::keys::constants::s => {
                        field.blue_right();
                    }
                    gdk::keys::constants::Left => {
                        field.red_left();
                    }
                    gdk::keys::constants::Right => {
                        field.red_right();
                    }
                    gdk::keys::constants::r => {
                        if field.state == GameState::WonByBlue || field.state == GameState::WonByRed
                        {
                            *field = GameInfo::new()
                        }
                    }
                    gdk::keys::constants::Escape => {
                        gtk::main_quit();
                    }
                    _ => {}
                };
                Inhibit(false)
            });
        }
        window.show_all();
    });
    app.run();
}
